"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _1 = require(".");
var TextInput = /** @class */ (function (_super) {
    __extends(TextInput, _super);
    function TextInput(element, selector) {
        var _this = _super.call(this, element, selector) || this;
        _this.element = element;
        return _this;
    }
    TextInput.prototype.type = function (text) {
        return this.element.sendKeys(text);
    };
    return TextInput;
}(_1.Element));
exports.TextInput = TextInput;
