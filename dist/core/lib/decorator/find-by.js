"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
function findBy(selector) {
    return function (target, propertyKey) {
        var type = Reflect.getMetadata('design:type', target, propertyKey);
        Object.defineProperty(target, propertyKey, {
            configurable: true,
            enumerable: true,
            get: function () {
                var promise = this.browser.findElement(selector);
                return new type(promise, selector);
            },
        });
    };
}
exports.findBy = findBy;
