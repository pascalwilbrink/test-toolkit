import { Key } from 'selenium-webdriver'

import { Page, Browser, findBy, TextInput, Button } from '../../../core/lib'

export class Homepage extends Page {

    @findBy('#lst-ib')
    private searchBar: TextInput

    @findBy('[name="btnK"]')
    private searchButton: Button

    constructor(
        browser: Browser
    ) {
        super(browser)
        super.setUrl('https://www.google.com')
    }

    async search(input: string): Promise<void> {
        await this.searchBar.type(input)
        this.findElement('#hplogo').then((logo) => {
            logo.click()

            this.searchButton.click()
        })
    //    await this.logo.click()
   //     await this.searchButton.click()
 //      await this.searchBar.type(Key.ENTER)
    }

}

