import { Page, findBy, Browser} from '../../../core/lib'
import { Checkbox } from '../../../core/lib/elements'

export class CheckboxPage extends Page {

    @findBy('input[type="checkbox"]')
    soccerInput: Checkbox

    constructor(
        browser: Browser
    ) {
        super(browser)
        super.setUrl('http://nativeformelements.com/')
    }

    async checkCheckbox(checked: boolean): Promise<void> {
        return await this.soccerInput.setValue(checked)
    }

}