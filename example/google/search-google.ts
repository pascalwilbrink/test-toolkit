import { Browser } from '../../core/lib'

import { Homepage } from './page/homepage'
import { CheckboxPage } from './page/checkbox-page';

const browser: Browser = new Browser('chrome')
const homePage: Homepage = new Homepage(browser)
const checkboxPage: CheckboxPage = new CheckboxPage(browser)

browser.open(checkboxPage)
    .then(() => {
        checkboxPage.checkCheckbox(true)
            .then(() => {
              //  checkboxPage.checkCheckbox(false)
            })
    })
/*
browser.open(homePage)
    .then(() => {
        homePage.search('Typescript is awesome!')
    })




    */