import { WebElementPromise } from 'selenium-webdriver'
import { Element } from '.'

export class Button extends Element {

    constructor(
        protected element: WebElementPromise,
        selector: string
    ) {
        super(element, selector)
    }

    async isDisabled() {
        try {
            return await this.element.getAttribute('disabled') === 'disabled'
        } catch (err) {
            return false
        }
    }

}