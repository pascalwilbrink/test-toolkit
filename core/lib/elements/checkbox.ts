import { WebElementPromise } from 'selenium-webdriver'
import { Element } from '.'

export class Checkbox extends Element {

    constructor(
        protected element: WebElementPromise,
        selector: string
    ) {
        super(element, selector)
    }

    async setValue(checked: boolean): Promise<void> {
        return this.getValue()
            .then((value) => {
                if (checked != value) {
                    this.click()
                }
            })
    }

    async getValue(): Promise<boolean> {
        return this.element.isSelected()
    }



}