import { WebElementPromise } from 'selenium-webdriver'
import { Element } from '.'

export class TextInput extends Element {

    constructor(
        protected element: WebElementPromise,
        selector: string
    ) {
        super(element, selector)
    }

    type(text: string) {
        return this.element.sendKeys(text)
    }

}