import { WebElementPromise } from 'selenium-webdriver'

export class Element {

    constructor(
        protected element: WebElementPromise,
        public selector: string
    ) {
    }

    async click() {
        try {
            return await this.element.click()
        } catch (err) {
            try {
                await this.element.getDriver().executeScript('arguments[0].click();', this.element)
            } catch (jsErr) {
                throw err
            }
        }
    }

    async isDisplayed() {
        try {
            return await this.element.isDisplayed()
        } catch (err) {
            return false
        }
    }

    async getText() {
        return await this.element.getText()
    }

}