import { Browser } from '.'

export class Page {

    private url: string

    constructor(
        protected browser: Browser
    ) { }

    setUrl(url: string) {
        this.url = url
    }

    getUrl(): string {
        return this.url
    }

    async findElement(selector: string): Promise<any> {
        return this.browser.findElement(selector)
    }

    async open(): Promise<void> {
        return this.browser.open(this)
    }

}
