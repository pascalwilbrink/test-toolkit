import 'chromedriver'

import { Builder, ThenableWebDriver, WebElement, By, WebElementPromise } from 'selenium-webdriver'

import { Page } from '.'

export class Browser {

    private driver: ThenableWebDriver

    constructor(
        private browserName: string
    ) {
        this.driver = new Builder()
            .forBrowser(this.browserName)
            .build()
    }

    async open(page: Page): Promise<void> {
        return await this.driver.navigate().to(page.getUrl())
    }

    async navigate(url: string): Promise<void> {
        return await this.driver.navigate().to(url)
    }

    findElement(selector: string): WebElementPromise {
        return this.driver.findElement(By.css(selector))
    }

    async close(): Promise<void> {
        return await this.driver.quit()
    }


}
